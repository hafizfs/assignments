package assignments.assignment1;

//Import Scanner
import java.util.Scanner;
import java.util.concurrent.SynchronousQueue;


//class ExtractNPM
public class ExtractNPM {
    //validate method
    public static boolean validate(long npm) {
        //Declare variable npmHolder dan mengubah long menjadi string
        String npmHolder= Long.toString(npm);
        //Checking length npm menggunakan .length()
        if(npmHolder.length() != 14) return false;
        //Check jurusan dengan memanggil method checkJurusan 
        String validMajor = checkJurusan(npmHolder);
        //lalu di check di if bawah tersebut
        if(validMajor.equals("Salah Jurusan")) return false;
        //Check umur jika hasil method checkUmur tidak true maka akan masuk ke dalam if menghasilkan false
        if(!checkUmur(npmHolder)) return false;
        //Check bagian Bagian E jika tidak true maka akan masuk ke if menghasilkan false
        if(!validasiNPM(npmHolder)) return false;
        

        return true;
    }
    //extract method
    public static String extract(long npm) {
        //declare variable hasil untuk mengubah long ke string menggunakan Long.toString()
        String hasil = Long.toString(npm); //holder npm agar mudah di panggil

        String jurusan= checkJurusan(hasil);
        String hari = hasil.substring(4,6);
        String bulan = hasil.substring(6,8);
        String tahun = hasil.substring(8, 12);
        String tahunMasuk = hasil.substring(0, 2);

        return "Tahun masuk: 20" + tahunMasuk + "\nJurusan: " + jurusan + "\nTanggal Lahir: " + hari + "-" + bulan + "-" + tahun;
    }

    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;
        while (!exitFlag) {
            long npm = input.nextLong();
            if (npm < 0) {
                exitFlag = true;
                break;
            }
            if (validate(npm)){
                System.out.print(extract(npm));
            } else{
                System.out.print("NPM tidak valid!");
            }       
        }
        input.close();
    }

    public static String checkJurusan (String npm){
        //menggunakan if else jika sesuai dengan if nya akan return hasil
        String hasil ="none";
        if (npm.substring(2, 4).equals("01")) {
            hasil = "Ilmu Komputer";
        } else if (npm.substring(2, 4).equals("02")){
            hasil = "Sistem Informasi";
        } else if (npm.substring(2, 4).equals("03")){
            hasil = "Teknologi Informasi";
        } else if (npm.substring(2, 4).equals("11")){
            hasil = "Teknik Telekomunikasi";
        } else if (npm.substring(2, 4).equals("12")){
            hasil = "Teknik Elektro";
        } else {
            hasil ="Salah Jurusan";
        }  
        return hasil;  
}
    // method checkUmur
    public static boolean checkUmur (String npm){
        int tahunMasuk = Integer.parseInt(npm.substring(0,2));
        int tahunLahir = Integer.parseInt(npm.substring(8, 12));
        int umurAnak = (tahunMasuk + 2000) - tahunLahir;// tambah 2000 karena hanya 2 nomor di akhir tahun yang di ambil

        //jika umurAnak tidak >= 15 akan menghasilkan false
        return umurAnak >= 15;
    }
    //Method untuk nge check bagian E NPM
    public static boolean validasiNPM (String npm){
        int a = Integer.parseInt(npm.substring(0, 1)) * Integer.parseInt(npm.substring(12,13));
        int b = Integer.parseInt(npm.substring(1, 2)) * Integer.parseInt(npm.substring(11,12));
        int c = Integer.parseInt(npm.substring(2, 3)) * Integer.parseInt(npm.substring(10,11));
        int d = Integer.parseInt(npm.substring(3, 4)) * Integer.parseInt(npm.substring(9,10));
        int e = Integer.parseInt(npm.substring(4, 5)) * Integer.parseInt(npm.substring(8,9));
        int f = Integer.parseInt(npm.substring(5, 6)) * Integer.parseInt(npm.substring(7,8));
        
        int jumlah = a + b + c + d + e + f + Integer.parseInt(npm.substring(6,7));
        System.out.println(jumlah);
        // jika jumlah nya >= 10 akan masuk ke while
        while (jumlah >= 10) {
            String jumlahStr = String.valueOf(jumlah); //menyimpan String.valueOf jumlah 
            jumlah -= jumlah;// variable jumlah dibuat jadi holder dengan cara di kurangin sesama
            for (int i = 0; i < jumlahStr.length(); i++) {
                    jumlah += Integer.parseInt(jumlahStr.substring(i, i+1)); // untuk melakukan metode 0,1
            }
        }
    // jika hasil String.valueOf jumlah tidak sama dengan npm.substring(13,14) maka akan berhasil false
    return String.valueOf(jumlah).equals(npm.substring(13,14));
    }
}

//teman diskusi
//Shadqi Marjan Sadiya 2006485964