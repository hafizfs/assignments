package assignments.assignment2;

import java.util.Arrays;
import java.util.Scanner;

import javax.xml.crypto.Data;

public class SistemAkademik {
    private static final int ADD_MATKUL = 1;
    private static final int DROP_MATKUL = 2;
    private static final int RINGKASAN_MAHASISWA = 3;
    private static final int RINGKASAN_MATAKULIAH = 4;
    private static final int KELUAR = 5;
    private static Mahasiswa[] daftarMahasiswa = new Mahasiswa[100];
    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    
    private Scanner input = new Scanner(System.in);
    //Method untuk mereturn npm mahasiswa yang sesuai
    private Mahasiswa getMahasiswa(long npm) {
        /* TODO: Implementasikan kode Anda di sini */

        for (int i =0; i <daftarMahasiswa.length; i++){
            if(daftarMahasiswa[i] != null){
                if (daftarMahasiswa[i].getNpm() == npm){
                    return daftarMahasiswa[i];
                }
            }
        }
        return null;
    }
    //Method untuk mereturn mata kuliah yang sesuai
    private MataKuliah getMataKuliah(String namaMataKuliah) {
        /* TODO: Implementasikan kode Anda di sini */
        for (int i =0; i <daftarMataKuliah.length; i++){
            if (daftarMataKuliah[i].getNamaMatkul().equals(namaMataKuliah)){
                System.out.println(daftarMataKuliah[i]);
                return daftarMataKuliah[i];
            }
        }
        return null;
    }
    //Method untuk Menambahkan Matkul
    private void addMatkul(){
        System.out.println("\n--------------------------ADD MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan ADD MATKUL : ");
        long npm = Long.parseLong(input.nextLine());
        Mahasiswa mahasiswa = getMahasiswa(npm); //mengambil nama mahasiswa
        /* TODO: Implementasikan kode Anda di sini 
        Jangan lupa lakukan validasi apabila banyaknya matkul yang diambil mahasiswa sudah 9*/

        System.out.print("Banyaknya Matkul yang Ditambah: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan nama matkul yang ditambah");
        for(int i=0; i<banyakMatkul; i++){
            System.out.print("Nama matakuliah " + i+1 + " : ");
            String namaMataKuliah = input.nextLine();
            MataKuliah matkul = getMataKuliah(namaMataKuliah);// mengambil nama MataKuliah
            /* TODO: Implementasikan kode Anda di sini */
            //Method untuk menambahkan matkul dan menambah jumlah matkul
            mahasiswa.validasiAddMatkulFinal(matkul);
            //method untuk menambahkan list mahasiswa
            matkul.addMahasiswa(mahasiswa);
        }
        System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
    }
    // Method untuk drop Matkul
    private void dropMatkul(){
        System.out.println("\n--------------------------DROP MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan DROP MATKUL : ");
        long npm = Long.parseLong(input.nextLine());
        Mahasiswa mahasiswa = getMahasiswa(npm); 

       /* TODO: Implementasikan kode Anda di sini 
        Jangan lupa lakukan validasi apabila mahasiswa belum mengambil mata kuliah*/
        if(mahasiswa.getTotalMatkul()>0){
            System.out.print("Banyaknya Matkul yang Di-drop: ");
            int banyakMatkul = Integer.parseInt(input.nextLine());
            System.out.println("Masukkan nama matkul yang di-drop:");
            for(int i=0; i<banyakMatkul; i++){
                System.out.print("Nama matakuliah " + i+1 + " : ");
                String namaMataKuliah = input.nextLine();
                MataKuliah matkul = getMataKuliah(namaMataKuliah);
                /* TODO: Implementasikan kode Anda di sini */
                mahasiswa.dropMatkul(matkul); //menghapus  matkul dari list matkul di mahasiswa 
                matkul.dropMahasiswa(mahasiswa); //menghapus mahasiswa dari list mahasiswa di mata kuliah
                
            }
        } else{
            System.out.println("[DITOLAK] Belum ada mata kuliah yang diambil.");
        }
        System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
    }

    private void ringkasanMahasiswa(){
        System.out.print("Masukkan npm mahasiswa yang akan ditunjukkan ringkasannya : ");
        long npm = Long.parseLong(input.nextLine());
        Mahasiswa mahasiswa = getMahasiswa(npm);
        // TODO: Isi sesuai format keluaran
        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama: " + mahasiswa.getNama());
        System.out.println("NPM: " + npm);
        System.out.println("Jurusan: " + mahasiswa.getJurusan());
        System.out.println("Daftar Mata Kuliah: ");

        /* TODO: Cetak daftar mata kuliah 
        Handle kasus jika belum ada mata kuliah yang diambil*/
        //mengecheck jika puya matkul akan masuk ke else , menggunakan getter
        if(mahasiswa.getTotalMatkul()== 0){
            System.out.println("Belum ada mata kuliah yang diambil");  
        }else{
            for (int i = 0; i<mahasiswa.getTotalMatkul();i++){
                System.out.println(i + 1 + ". "+ mahasiswa.getMatakuliah()[i].getNamaMatkul());
            }
        }
        System.out.println("Total SKS: " + mahasiswa.getTotalSks());
        System.out.println("Hasil Pengecekan IRS:");
        /* TODO: Cetak hasil cek IRS
        Handle kasus jika IRS tidak bermasalah */
        mahasiswa.cekIRS();
    }

    private void ringkasanMataKuliah(){
        System.out.print("Masukkan nama mata kuliah yang akan ditunjukkan ringkasannya : ");
        String namaMataKuliah = input.nextLine();
        MataKuliah matkul = getMataKuliah(namaMataKuliah);
        // TODO: Isi sesuai format keluaran
        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama mata kuliah: " + matkul.getNamaMatkul());
        System.out.println("Kode: " + matkul.getKode());
        System.out.println("SKS: " + matkul.getSKS());
        System.out.println("Jumlah mahasiswa: " + matkul.getTotalMahasiswa());
        System.out.println("Kapasitas: " + matkul.getKapasitas());
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini: ");
       /* TODO: Cetak hasil cek IRS
        Handle kasus jika tidak ada mahasiswa yang mengambil */
        //sama kayak di atas bedanya kita pakek method yang ada di matakuliah.java
        if(matkul.getTotalMahasiswa() == 0){
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini. ");
        }else{
            for(int i = 0; i < matkul.getTotalMahasiswa(); i++){
                System.out.println((i+1) + ". " + (matkul.getDaftarMahasiswa()[i].getNama()));
            }
        }
    }

    private void daftarMenu(){
        int pilihan = 0;
        boolean exit = false;
        while (!exit) {
            System.out.println("\n----------------------------MENU------------------------------\n");
            System.out.println("Silakan pilih menu:");
            System.out.println("1. Add Matkul");
            System.out.println("2. Drop Matkul");
            System.out.println("3. Ringkasan Mahasiswa");
            System.out.println("4. Ringkasan Mata Kuliah");
            System.out.println("5. Keluar");
            System.out.print("\nPilih: ");
            try {
                pilihan = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                continue;
            }
            System.out.println();
            if (pilihan == ADD_MATKUL) {
                addMatkul();
            } else if (pilihan == DROP_MATKUL) {
                dropMatkul();
            } else if (pilihan == RINGKASAN_MAHASISWA) {
                ringkasanMahasiswa();
            } else if (pilihan == RINGKASAN_MATAKULIAH) {
                ringkasanMataKuliah();
            } else if (pilihan == KELUAR) {
                System.out.println("Sampai jumpa!");
                exit = true;
            }
        }

    }


    private void run() {
        System.out.println("====================== Sistem Akademik =======================\n");
        System.out.println("Selamat datang di Sistem Akademik Fasilkom!");
        
        System.out.print("Banyaknya Matkul di Fasilkom: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan matkul yang ditambah");
        System.out.println("format: [Kode Matkul] [Nama Matkul] [SKS] [Kapasitas]");
        // method untuk menyimpan daftar matakuliah dan mahasiswa di array yang telah dibikin
        for(int i=0; i<banyakMatkul; i++){
            String[] dataMatkul = input.nextLine().split(" ", 4);
            int sks = Integer.parseInt(dataMatkul[2]);
            int kapasitas = Integer.parseInt(dataMatkul[3]);
            /* TODO: Buat instance mata kuliah dan masukkan ke dalam Array */
            //berguna untuk mengupdate/menyimpan array yang berisi daftarMatakuliah 
            MataKuliah matkul = new MataKuliah(dataMatkul[0], dataMatkul[1], sks, kapasitas);
            daftarMataKuliah[i] = matkul;

            
        }

        System.out.print("Banyaknya Mahasiswa di Fasilkom: ");
        int banyakMahasiswa = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan data mahasiswa");
        System.out.println("format: [Nama] [NPM]");

        for(int i=0; i<banyakMahasiswa; i++){
            String[] dataMahasiswa = input.nextLine().split(" ", 2);
            long npm = Long.parseLong(dataMahasiswa[1]);
            /* TODO: Buat instance mata kuliah dan masukkan ke dalam Array */
            //berguna untuk mengupdate daftarMahasiswa dengan araya mahasiswa 
            Mahasiswa mahasiswa = new Mahasiswa(dataMahasiswa[0], npm);
            daftarMahasiswa[i]= mahasiswa;
        }

        daftarMenu();
        input.close();
    }

    public static void main(String[] args) {
        SistemAkademik program = new SistemAkademik();
        program.run();
    }


    
}
