package assignments.assignment2;

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private Mahasiswa[] daftarMahasiswa;
    private int totalMahasiswa;

    public MataKuliah(String kode, String nama, int sks, int kapasitas){
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.kode = kode;
        this.sks = sks;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa [kapasitas];//implementasi benar
    }
    //method getter
    public String getKode(){
        return this.kode;
    }
    public String getNamaMatkul(){
        return this.nama; //nama matkul
    }
    public boolean matkulPenuh(){
        return this.kapasitas == this.totalMahasiswa;
    }
    public int getSKS(){
        return this.sks;
    }
    public int getKapasitas(){
        return this.kapasitas;
    }
    public int getTotalMahasiswa(){
        return this.totalMahasiswa;
    }
    public Mahasiswa[] getDaftarMahasiswa(){
        return this.daftarMahasiswa;
    }
    //method untuk add mahasiswa konsep sama addmatkul
    public void addMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        this.daftarMahasiswa[this.totalMahasiswa++] = mahasiswa;

        // for (int i = 0; i<this.daftarMahasiswa.length;i++){
        //     if (this.daftarMahasiswa[i] == null){
        //         this.daftarMahasiswa[i] = mahasiswa;
        //         this.totalMahasiswa++;
        //         break;
        //     } 
        // }
    }
    //method untuk drop mahasiswa
    public void dropMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        Mahasiswa[] hasil = new Mahasiswa[this.daftarMahasiswa.length];
        int indeks = 0;
        for(int i = 0; i< this.daftarMahasiswa.length; i++){
            //ketika dipilih syahdan berarti bakal masuk ke if
            if (this.daftarMahasiswa[i] != null){
                if (!this.daftarMahasiswa[i].getNama().equals(mahasiswa.getNama())){
                    //kalau gk pakek indeks, posisi devin bakal jadi null (urutan syahdan,devin,bintang)
                    hasil[indeks] = this.daftarMahasiswa[i];
                    //indeks berguna agar hasil urutan nya (syahdan, bintang,null) bukan (syahdan,null,bintang)
                    indeks += 1;
                } 
            }
        }
        this.daftarMahasiswa = hasil;
        this.totalMahasiswa --; //mengurangi total mahasiswa
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return this.nama;
    }
}
