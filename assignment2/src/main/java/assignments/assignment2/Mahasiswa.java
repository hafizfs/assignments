package assignments.assignment2;

import java.lang.reflect.Array;

public class Mahasiswa {
    private MataKuliah[] mataKuliah = new MataKuliah[10];
    private String[] masalahIRS;
    private int totalSKS;
    private String nama;
    private String jurusan;
    private long npm;
    private int totalMatkul;
    private String kodeJurusan;

    //inisiator 
    public Mahasiswa(String nama, long npm){
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.npm = npm;
        checkJurusan(npm); 

    }
    //getter 
    public String getJurusan() {
        return this.jurusan;
    }
    public int getTotalSks() {
        return this.totalSKS;
    }
    public int getTotalMatkul(){
        return this.totalMatkul;
    }
    public long getNpm(){
        return this.npm;
    }
    public String getNama(){
        return this.nama;
    }
    public String[] getMasalahIRS(){
        return this.masalahIRS;
    }
    public MataKuliah[] getMatakuliah(){
        return this.mataKuliah;
    }
    //method untuk menambah matkul
    public void addMatkul(MataKuliah matkul){
        //Iterasi for loop untuk mengisi array matakuliah jika matakuliah masih null
        for (int i = 0; i<this.getMatakuliah().length;i++){
            if (this.mataKuliah[i] == null){
                this.mataKuliah[i] = matkul;
                this.totalMatkul++; //buat nambahin total matkul
                this.totalSKS += matkul.getSKS();
                //di break biar gk ke isi semua cuma satu aja yang ke isi
                break;
            } 
            
        }

    }
    //method untuk nge check jika matkul di atas 9 akan false atau tidak akan dilaksanakan
    public boolean validasiAdd1(){
        int hasil = 0;
        for (int i = 0; i<this.mataKuliah.length; i++){
            if (this.mataKuliah[i] == null){
                continue;
            }else {
                hasil++;
            }
        }
        return hasil<10;
    }
    //method validasi untuk ngecheck ada atau gk nama matakuliah di daftar mata kuliah
    public boolean validasiAddDrop2(MataKuliah mataKuliah){
        for (int i = 0; i<this.getMatakuliah().length;i++){
            if (this.getMatakuliah()[i]!=null){
                if (this.getMatakuliah()[i].getNamaMatkul().equals(mataKuliah.getNamaMatkul())){
                    return false;
                }
            }
        }
        return true; 
    }
    //method untuk nge check jika matakuliah penuh atau gk
    public boolean validasiAdd3(MataKuliah mataKuliah){
        if (mataKuliah.matkulPenuh()){
            return false;
        }
        return true;
    }

    //method untuk nge validasi sekaligus menambah matkul dan akan sysout sesuai validasi di pdf
    public void validasiAddMatkulFinal(MataKuliah mataKuliah){
        if (validasiAddDrop2(mataKuliah)){
            if (validasiAdd3(mataKuliah)){
                if (validasiAdd1()){
                    this.addMatkul(mataKuliah);
                } else {
                    System.out.println("[DITOLAK] Maksimal mata kuliah yang diambil hanya 10.");
                }
            } else {
                System.out.println("[DITOLAK] " + mataKuliah.getNamaMatkul() + " telah penuh kapasitasnya.");
            }
        } else {
            System.out.println("[DITOLAK] " + mataKuliah.getNamaMatkul() + " telah diambil sebelumnya.");
        }
    }
    // Method untuk drop matkul
    public void dropMatkul(MataKuliah mataKuliah){
        /* TODO: implementasikan kode Anda di sini */
        // jika false maka akan masuk ke if karena untuk drop matkul, matakuliah nya harus udah ada
        if(!validasiAddDrop2(mataKuliah)){
            for (int i = 0; i < this.mataKuliah.length; i++){
                //mengganti matkul yang udah ada jadi null
                if (this.mataKuliah[i] != null){
                    if(this.mataKuliah[i].getNamaMatkul().equals(mataKuliah.getNamaMatkul())){
                        int n = i;
                        //biar gk index OOR maka di kurang 1
                        while(n <this.mataKuliah.length-1){
                            //array di geser 
                            this.mataKuliah[n] = this.mataKuliah[n+1];
                            n++;
                        } 
                        // untuk mengubah matakuliah yang di drop jadi null
                        this.mataKuliah[this.mataKuliah.length-1] = null; 
    
                        //Contoh Skenario
                        // mtk fisika kimia bindo
                        // drop fisika
                        // n = 1
                        // masuk while (1 < 3 ) //
                        // #1 fisika = kimia
                        // #2 kimia = bindo
                        // indeks ke 3 = null
                        // mtk, kimia, bindo, null
                    }  
                }
            } 
        }else{
            System.out.println("[DITOLAK] " + mataKuliah.getNamaMatkul() + " belum pernah diambil.");
        }
        this.totalMatkul --;
        this.totalSKS -= mataKuliah.getSKS();
    }


    public void cekIRS(){
        /* TODO: implementasikan kode Anda di sini */
        masalahIRS = new String[2];
        int urutan = 0;
        //menggunakan indeksing agar urutan nya tidak salah
        if(this.totalSKS>24){
            this.masalahIRS[urutan] = "SKS yang Anda ambil lebih dari 24";
            urutan++;
        }
        //looping untuk nge check jika matkul yang dipilih IK atau SI
        for (int i = 0; i<this.mataKuliah.length; i++){
            if (this.mataKuliah[i] == null){
                continue;
            } else{
                if (this.mataKuliah[i].getKode().equals("IK")){
                    if(this.getJurusan().equals("Ilmu Komputer")){
                        continue;
                    } else{
                        this.masalahIRS[urutan] = "Mata Kuliah " + this.mataKuliah[i].getNamaMatkul() + " tidak dapat diambil jurusan " + this.kodeJurusan; 
                        urutan++;  
                    }
                }else if(this.mataKuliah[i].getKode().equals("SI")){
                    if(this.getJurusan().equals("Sistem Informasi")){
                        continue;
                    }else{
                        this.masalahIRS[urutan] = "Mata Kuliah " + this.mataKuliah[i].getNamaMatkul() + " tidak dapat diambil jurusan " + this.kodeJurusan; 
                        urutan++; 
                    }
                }
            }
        }

        //untuk ngeprint irs bermasalah atau tidak
        if ( belumAdaIRS() == 20){
            System.out.println("IRS tidak bermasalah.");
        }else{
            int counter = 1;
            for (int i = 0; i<this.masalahIRS.length; i++){
                if (this.masalahIRS[i] == null){
                    continue;
                } else{
                    System.out.println((counter) + ". " + this.masalahIRS[i]);
                    counter++;
                }
            }
            
        }      
    }
    //method untuk nge check IRS ada isinya atau tidak
    public int belumAdaIRS(){
        int counter = 0;
        for (int i =0 ; i<this.masalahIRS.length;i++){
            if (this.masalahIRS[i] == null){
                counter++;
            }else{
                continue;
            }
        }
        return counter;
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return this.nama;
    }
    //method untuk return jurusan mahasiswa
    public void checkJurusan (long npm){
        String hasil = String.valueOf(npm);
        if (hasil.substring(2, 4).equals("01")) {
            this.jurusan = "Ilmu Komputer";
            this.kodeJurusan = "IK";
        } else if (hasil.substring(2, 4).equals("02")){
            this.jurusan = "Sistem Informasi";
            this.kodeJurusan = "SI";
        }
    }
}
