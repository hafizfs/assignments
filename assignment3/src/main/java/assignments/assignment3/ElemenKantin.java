package assignments.assignment3;

class ElemenKantin extends ElemenFasilkom {
    
    //Datafield
    private Makanan[] daftarMakanan = new Makanan[10];
    private int jumlahMakanan = 0;
    //Constructor
    public ElemenKantin(String nama) {
        this.setNama(nama);
        this.setTipe("ElemenKantin");;
    }
    //Method Getter
    public int jumlahMakanan(){
        return this.jumlahMakanan;
    }
    public Makanan[] getDaftarMakanan(){
        return this.daftarMakanan;
    }
    //Method setter untuk Array elemenKantin makanan yang di jual
    public void setMakanan(String nama, long harga) {
        if(!adaMakanan(nama)){
            daftarMakanan[jumlahMakanan] = new Makanan(nama,harga);
            System.out.printf("%s telah mendaftarkan %s dengan harga %d\n", this.toString(), nama, harga);
            jumlahMakanan++;
        } else{
            System.out.printf("[DITOLAK] %s sudah pernah terdaftar", nama);
        }
    }
    //Method getter Array makanan ElemenKantin
    public Makanan chekMakanan(String nama) {
        for (int i = 0; i < 10; i++) {
            if (this.daftarMakanan[i] != null) {
                if (nama.equals(this.daftarMakanan[i].toString())) {
                    return this.daftarMakanan[i];
                }
            }
        }
        return null;
    }
    //Method untuk check ada atau tidak object makanan dalam array ElemenKantin Makanan
    public boolean adaMakanan(String makanan){
        boolean adaMakanan = false;
        for (int i=0; i<10; i++){
            if(daftarMakanan[i] != null && daftarMakanan[i].toString().equals(makanan)){
                adaMakanan = true;
                break;
            }
        }
        return adaMakanan;
    }

}