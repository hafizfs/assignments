package assignments.assignment3;

class Makanan {

    //Datafield
    private String nama;

    private long harga;

    public Makanan(String nama, long harga) {
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.harga = harga;
    }
    public String getNama(){
        return this.nama;
    }
    public String toString() {
        return this.nama;
    }
    public long getHarga(){
        return this.harga;
    }
}