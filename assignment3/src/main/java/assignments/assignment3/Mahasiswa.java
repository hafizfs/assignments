package assignments.assignment3;

class Mahasiswa extends ElemenFasilkom {
    
    //Data Field Class Mahasiswa
    private MataKuliah[] daftarMataKuliah = new MataKuliah[10];
    
    private long npm;

    private String tanggalLahir;
    
    private String jurusan;

    private int jumlahMatkul;

    //Constructur class Mahasiswa
    public Mahasiswa(String nama, long npm) {
        this.setNama(nama);
        this.setTipe("Mahasiswa");
        this.npm = npm;
        this.jurusan = this.extractJurusan(npm);
        this.tanggalLahir = this.extractTanggalLahir(npm);
    }

    public void addMatkul(MataKuliah mataKuliah) {
        for (int i = 0; i < daftarMataKuliah.length; i++) {
            if (mataKuliah == daftarMataKuliah[i]){ // jika Pernah terdaftar
                System.out.println("[DITOLAK] " + mataKuliah.toString() + " telah diambil sebelumnya");
                return;
            }
        }
        if(mataKuliah.getJumlahStudent() == mataKuliah.getKapasitas()){ //jika kapasitas full
            System.out.printf("[DITOLAK] %s telah penuh kapasitasnya\n", mataKuliah.getNama());
        } else { //menambahkan matkul
            this.daftarMataKuliah[jumlahMatkul++]= mataKuliah;
            mataKuliah.addMahasiswa(this);
            System.out.printf("%s berhasil menambahkan mata kuliah %s\n", this.getNama(), mataKuliah.getNama());
        }
    }
    public void dropMatkul(MataKuliah mataKuliah) {
        int marker = 0;
        MataKuliah[] temp = new MataKuliah[10]; //membuat array copy
        //mencari matkul yang ingin di drop
        for (int i = 0; i<10; i++){ 
            if (mataKuliah == daftarMataKuliah[i]){
                System.out.printf("%s berhasil drop mata kuliah %s\n", this.getNama(), mataKuliah.getNama());
            } else {
                //yang tidak sesuai dengan input bakal di masukin ke temp
                temp[marker++]= daftarMataKuliah[i];
            }
        }
        //jika marker==10 maka tidak ada yang di remove
        if (marker == 10){ 
            System.out.printf("[DITOLAK] %s belum pernah diambil\n", mataKuliah.getNama());
        }
    }
    //Mahasiswa mensapa dosen
    public void dosenSapa(Dosen dosen) {
        for (int i = 0; i < this.jumlahMatkul; i++) {
            if (this.daftarMataKuliah[i] != null && dosen.getMataKuliah() == this.daftarMataKuliah[i]) {
                this.setFriendship(this.getFriendship()+2);
                dosen.setFriendship(dosen.getFriendship()+2);
            }
        }
    }
    //method extract Tanggal lahir
    private String extractTanggalLahir(long npm) {
        //mengubah long menjadi 
        int tahun = (int) ((npm/100) % 10000);
        int bulan = (int) ((npm/1000000) % 100);
        int hari = (int) ((npm/100000000) % 100);
        return String.format("%d-%d-%s", hari, bulan, tahun);
    }
    
    private String extractJurusan(long npm) {
        String npmProses = String.valueOf(npm); //NPM dalam string
        String kodeJurusan= npmProses.substring(2,4);
        //if else untuk menentukan jurusan
        if (kodeJurusan.equals("01")){
            return "Ilmu Komputer";
        }else{
            return "Sistem Informasi";
        }
    }
    //Method Getter
    public String getJurusan(){
        return jurusan;
    }
    public long getNpm(){
        return npm;
    }
    public MataKuliah[] getDaftarMatkul() {
        return this.daftarMataKuliah;
    }
    public int getJumlahMatkul() {
        return this.jumlahMatkul;
    }
    public String getTanggalLahir() {
        return this.tanggalLahir;
    }

}