package assignments.assignment3;

class Dosen extends ElemenFasilkom {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */
    //Datafield
    private MataKuliah mataKuliah;
    //constructor
    //constrotur
    public Dosen(String nama) {
        this.setNama(nama);
        this.setTipe("Dosen");
    }
    //Method getter MataKuliah
    public MataKuliah getMataKuliah() {
        return this.mataKuliah;
    }
    //Method mengajar MataKuliah
    public void mengajarMataKuliah(MataKuliah mataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        if (this.mataKuliah != null){
            System.out.printf("[DITOLAK] %s sudah mengajar mata kuliah %s\n", this, this.mataKuliah);
        } else if(mataKuliah.getDosen() != null){
            System.out.printf("[DITOLAK] %s sudah memiliki dosen pengajar\n", mataKuliah);
        } else{
            this.mataKuliah = mataKuliah;
            mataKuliah.addDosen(this);
            System.out.printf("%s mengajar mata kuliah %s\n", this, mataKuliah);
        }
    }
    //Method drop Matakuliah
    public void dropMataKuliah() {
        /* TODO: implementasikan kode Anda di sini */
        if (mataKuliah != null){
            System.out.printf("%s berhenti mengajar %s\n", this, this.mataKuliah);
            mataKuliah.dropDosen();//jangan lupa di drop dosen
            this.mataKuliah = null;//jika terpilih maka mataKuliah ini akan menjadi null
        } else if(mataKuliah == null){
            System.out.printf("[DITOLAK] %s sedang tidak mengajar mata kuliah apapun\n", this);
        }
    } 
    
}
