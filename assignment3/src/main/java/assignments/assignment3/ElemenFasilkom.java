package assignments.assignment3;

abstract class ElemenFasilkom {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */
    //Data field
    private String tipe;
    
    private String nama;

    private int friendship;

    private int totalSapa = 0; //jumlah orang yang disapa

    private ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];

    //Constructur 
    public void elemen (String nama, String tipe){
        this.nama = nama;
        this.tipe = tipe;
    }

    //Method menyapa
    public void menyapa(ElemenFasilkom elemenFasilkom){
        //check jika sudah menyapa maka di tolak
        if (checkTelahMenyapa(elemenFasilkom)){
            System.out.printf("[DITOLAK] %s telah menyapa %s hari ini\n",this.getNama(), elemenFasilkom.getNama());
        } else{
            for (int i = 0; i < this.telahMenyapa.length; i++){
                if (this.telahMenyapa[i] == null){
                    this.telahMenyapa[i] = elemenFasilkom;
                    System.out.printf("%s menyapa dengan %s \n", this.nama, elemenFasilkom.getNama());
                    //check untuk friendship ranking
                    if(this.tipe.equals("Mahasiswa") && elemenFasilkom.getTipe().equals("Dosen")){
                        ((Mahasiswa) this).dosenSapa((Dosen)elemenFasilkom);
                    } else if(this.tipe.equals("Dosen") && elemenFasilkom.getTipe().equals("Mahasiswa")){
                        ((Mahasiswa) elemenFasilkom).dosenSapa((Dosen) this);
                    }
                    //menambahkan total sapa 
                    this.totalSapa++;
                    for (int j = 0; j < elemenFasilkom.getTelahMenyapa().length; j++) {
                        if (elemenFasilkom.getTelahMenyapa()[j] == null) {
                            elemenFasilkom.getTelahMenyapa()[j] = this;
                            elemenFasilkom.totalSapa++;
                            break;
                        }
                    }
                    break;
                }
            }
        }
    }
    //Method untuk mengecheck menyapa setengah orang di  fasilkom atau tidak
    public void checkSetengahMenyapa(int orang){
        //Agar Genap Ganjil ter Handle menggunakan this.totalSapa*2
        if (this.totalSapa*2 >= orang - 1 ) {
            this.setFriendship(this.getFriendship()+10);
        } else {
            this.setFriendship(this.getFriendship()-5);
        }
    }
    //method untuk mengecheck sudah menyapa atau belum
    public boolean checkTelahMenyapa(ElemenFasilkom elemenFasilkom){
        boolean check = false;
        for (int i = 0; i < this.telahMenyapa.length; i++){
            if (telahMenyapa[i] == elemenFasilkom){
                check = true;
                break;
            } 
        }
        return check;
    }  
    //untuk mereset array menyapa dan mengecheck setengah menyapa
    public void resetMenyapa() {
        checkSetengahMenyapa(Main.getTotalElemen());
        this.telahMenyapa = new ElemenFasilkom[100];
        this.totalSapa = 0;
    }
    //Method untuk Membeli makanan
    public void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) {
        ElemenKantin namaPenjual = (ElemenKantin) penjual;
        if (namaPenjual.adaMakanan(namaMakanan)){
            Makanan makanan = namaPenjual.chekMakanan(namaMakanan);
            pembeli.setFriendship(pembeli.getFriendship()+1);
            namaPenjual.setFriendship(namaPenjual.getFriendship()+1);
            System.out.printf("%s berhasil membeli %s seharga %d\n", pembeli.getNama(), namaMakanan, makanan.getHarga());
        } else {
            System.out.printf("[DITOLAK] %s tidak menjual %s\n", penjual.getNama(), namaMakanan);
        }
        
    }
    //Method Getter dan Setter
    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return nama;
    }
    public String getNama(){
        return this.nama;
    }
    public String getTipe(){
        return this.tipe;
    }

    public int getFriendship(){
        return this.friendship;
    }

    public int getSapa(){
        return this.totalSapa;
    }
    public ElemenFasilkom[] getTelahMenyapa() {
        return this.telahMenyapa;
    }

    public void setNama(String nama){
        this.nama = nama;
    }
    
    public void setTipe(String tipe){
        this.tipe = tipe;
    }

    public void setFriendship (int friendship){
        this.friendship = friendship;
        if (friendship>100){
            this.friendship = 100;
        } else if (friendship <0){
            this.friendship = 0;
        }
    }
}