package assignments.assignment3;

class MataKuliah {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private String nama;
    
    private int kapasitas;

    private Dosen dosen;

    private Mahasiswa[] daftarMahasiswa;
    
    private int jumlahStudent; //total jumlah student

    public MataKuliah(String nama, int kapasitas) {
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[kapasitas];
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        this.daftarMahasiswa[this.jumlahStudent++] = mahasiswa;
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        int indeks = 0;
        //mencari mahasiswa di daftarMahasiswa lalu di tandai dengan indeks
        for (int i = 0; i < daftarMahasiswa.length; i++) {
            if (daftarMahasiswa[i] == mahasiswa) {
                indeks = i;
            }
        }
        //menggeser ke kiri
        for (int j = indeks; j < daftarMahasiswa.length -1; j++) {
            daftarMahasiswa[j] = daftarMahasiswa[j+1];
        }
        //mengubah array paling belakang menjadi null
        daftarMahasiswa[daftarMahasiswa.length-1] = null;
        this.jumlahStudent--;
    }

    public void addDosen(Dosen dosen) {
        this.dosen = dosen; 
    }

    public void dropDosen() {
        this.dosen = null;
    }
    //method getter
    public int getJumlahStudent() {
        return this.jumlahStudent;
    }
    public int getKapasitas() {
        return this.kapasitas;
    }
    public Dosen getDosen(){
        return this.dosen;
    }
    public Mahasiswa[] getDaftarMahasiswa() {
        return this.daftarMahasiswa;
    }
    public String getNama() {
        return this.nama;
    }
    public String toString() {
        return this.nama;
    }

}