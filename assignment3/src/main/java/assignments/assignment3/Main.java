package assignments.assignment3;

import java.util.Scanner;

public class Main {


    //Data Field class Main
    private static ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];

    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];

    private static int totalMataKuliah = 0;

    private static int totalElemenFasilkom = 0; 
    //Method untuk addMahasiswa, addDosen, addElemenKantin 
    public static void addMahasiswa(String nama, long npm) {
        //Membuat instance baru dari class ElemenFasilkom dan memasukan instance/object baru ke dalam array
        daftarElemenFasilkom[totalElemenFasilkom] = (ElemenFasilkom) new Mahasiswa(nama, npm);
        System.out.printf("%s berhasil ditambahkan\n", nama);
        totalElemenFasilkom++;
    }

    public static void addDosen(String nama) {
        //Membuat instance baru dari class ElemenFasilkom dan memasukan instance/object baru ke dalam array
        daftarElemenFasilkom[totalElemenFasilkom] = (ElemenFasilkom) new Dosen(nama);
        System.out.printf("%s berhasil ditambahkan\n", nama);
        totalElemenFasilkom++;
    }

    public static void addElemenKantin(String nama) {
        //Membuat instance baru
        daftarElemenFasilkom[totalElemenFasilkom] = new ElemenKantin(nama);
        System.out.printf("%s berhasil ditambahkan\n", nama);
        totalElemenFasilkom++;
    }
    //Method untuk Menyapa
    public static void menyapa(String objek1, String objek2) {
        //Jika tidak sama maka akan di tolak
        if (objek1.equals(objek2)) {
            System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa");
        } else {
            ElemenFasilkom elemen1 = cariElemen(objek1);
            ElemenFasilkom elemen2 = cariElemen(objek2);
            elemen1.menyapa(elemen2);
        }
    }

    public static void addMakanan(String objek, String namaMakanan, long harga) {
        ElemenFasilkom elemenPenjual = cariElemen(objek);
        if (!elemenPenjual.getTipe().equals("ElemenKantin")) {
            System.out.printf("[DITOLAK] %s bukan merupakan elemen kantin\n", objek);
        } else{
            ElemenKantin elemenKantin = (ElemenKantin) elemenPenjual;
            elemenKantin.setMakanan(namaMakanan, harga);
        }

    }
    //Method untuk membeli Makanan
    public static void membeliMakanan(String objek1, String objek2, String namaMakanan) {
        ElemenFasilkom elemen1 = cariElemen(objek1);
        ElemenFasilkom elemen2 = cariElemen(objek2);
        if (!elemen2.getTipe().equals("ElemenKantin")) {
            System.out.println("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan");
        } else if (objek1.equals(objek2)) {
            System.out.println("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri");
        } else {
            elemen1.membeliMakanan(elemen1, elemen2, namaMakanan);
        }
    }
    //Method untuk menambahkan Matkul
    public static void createMatkul(String nama, int kapasitas) {
        daftarMataKuliah[totalMataKuliah] = new MataKuliah(nama, kapasitas);
        System.out.printf("%s berhasil ditambahkan dengan kapasitas %d\n", nama, kapasitas);
        totalMataKuliah++;
    }
    //Method untuk nemabahkan Matkul
    public static void addMatkul(String objek, String namaMataKuliah) {  
        //membuat instance yang berisi objek sesuai parameter yang diberikan 
        ElemenFasilkom elemenMahasiswa = cariElemen(objek);
        MataKuliah matakuliah= cariMataKuliah(namaMataKuliah);
        if (!elemenMahasiswa.getTipe().equals("Mahasiswa")) {
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");
        }else{
            ((Mahasiswa)elemenMahasiswa).addMatkul(matakuliah);
        }
    }
    //Method Drop Matkul
    public static void dropMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom elemenMahasiswa = cariElemen(objek);
        MataKuliah matakuliah= cariMataKuliah(namaMataKuliah);
        //jika object bukan mahasiswa maka akan ditolak
        if (!elemenMahasiswa.getTipe().equals("Mahasiswa")) {
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");
        }else{
            ((Mahasiswa)elemenMahasiswa).dropMatkul(matakuliah);
        }
    }

    public static void mengajarMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom elemenDosen = cariElemen(objek);
        MataKuliah matakuliah= cariMataKuliah(namaMataKuliah);
        if (!elemenDosen.getTipe().equals("Dosen")) {
            System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");
        }else{
            ((Dosen)elemenDosen).mengajarMataKuliah(matakuliah);
        }
    }

    public static void berhentiMengajar(String objek) {
        ElemenFasilkom elemenDosen = cariElemen(objek);
        if(elemenDosen.getTipe().equals("Dosen")){ 
            Dosen dosen = (Dosen) elemenDosen;
            dosen.dropMataKuliah();
        }else{
            System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");
        }
    }
    //Method untuk nge print ringkasan Mahasiswa
    public static void ringkasanMahasiswa(String objek) {
        ElemenFasilkom elemenMahasiswa = cariElemen(objek);
        if (!elemenMahasiswa.getTipe().equals("Mahasiswa")) {
            System.out.printf("[DITOLAK] %s bukan merupakan seorang mahasiswa\n", objek);
        }else {
            Mahasiswa mahasiswa = (Mahasiswa) elemenMahasiswa;
            System.out.println("Nama: " + mahasiswa.getNama());
            System.out.println("Tanggal Lahir: " + mahasiswa.getTanggalLahir());
            System.out.println("Jurusan: " + mahasiswa.getJurusan());
            System.out.println("Daftar Mata Kuliah:");
            MataKuliah[] daftarMatkul = mahasiswa.getDaftarMatkul();
            if(mahasiswa.getJumlahMatkul()==0){
                System.out.println("Belum ada mata kuliah yang diambil");
            } else{
                for (int i = 0; i <mahasiswa.getJumlahMatkul();i++){
                    System.out.printf("%d. %s\n", (i+1), daftarMatkul[i]);
                }
            }

        }
    }
    //Method Print Ringkasan MataKuliah
    public static void ringkasanMataKuliah(String namaMataKuliah) {
        MataKuliah matkul = cariMataKuliah(namaMataKuliah);
        String dosen;
        //Checker ada dosen atau tidak
        if(matkul.getDosen() != null){
            dosen = matkul.getDosen().getNama();
        } else{
            dosen = "Belum ada";
        }
        System.out.println("Nama mata kuliah: " + matkul.getNama());
        System.out.println("Jumlah mahasiswa: " + matkul.getJumlahStudent());
        System.out.println("Kapasitas: " + matkul.getKapasitas());
        System.out.println("Dosen pengajar: " + dosen);
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini:");
        Mahasiswa[] mahasiswa = matkul.getDaftarMahasiswa();
        if(matkul.getJumlahStudent()==0){
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini");
        } else{
            for (int i = 0; i<matkul.getJumlahStudent();i++){
                System.out.printf("%d. %s\n", (i+1), mahasiswa[i].getNama());
            }
        }

    }


    public static void nextDay() {
        /* TODO: implementasikan kode Anda di sini */
        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate");
        for (int i = 0; i < totalElemenFasilkom; i++) {
            daftarElemenFasilkom[i].resetMenyapa();
        }
        friendshipRanking();
    }
    // Method untuk shorting
    public static void friendshipRanking() {
        for (int i = 0; i < totalElemenFasilkom; i++) {
            for (int j = i+1; j < totalElemenFasilkom; j++) {
                ElemenFasilkom temp;
                if (daftarElemenFasilkom[i] != null && daftarElemenFasilkom[j] != null) {
                    //If untuk shorting friendship ranking
                    if (daftarElemenFasilkom[i].getFriendship() < daftarElemenFasilkom[j].getFriendship()) {
                        //geser array agar urutan nya dari kiri ke kanan (besar ke kecil)
                        temp = daftarElemenFasilkom[i];
                        daftarElemenFasilkom[i] = daftarElemenFasilkom[j];
                        daftarElemenFasilkom[j] = temp;
                    
                    } else if (daftarElemenFasilkom[i].getFriendship() == daftarElemenFasilkom[j].getFriendship()) {
                        //If untuk shorting nama sesuai alphabet
                        if ((daftarElemenFasilkom[i].getNama()).compareTo(daftarElemenFasilkom[j].getNama()) > 0) {
                            //geser array agar urutan nya dari kiri ke kanan (besar ke kecil)
                            temp = daftarElemenFasilkom[i];
                            daftarElemenFasilkom[i] = daftarElemenFasilkom[j];
                            daftarElemenFasilkom[j] = temp;
                        }
                    }
                }
            }
        }
        //untuk nge print sesuai urutan yang sudah di short di atas
        for (int i = 0; i < totalElemenFasilkom; i++) {
            System.out.printf("%d. %s(%d)\n", (i+1), daftarElemenFasilkom[i].getNama(), daftarElemenFasilkom[i].getFriendship());
        }
    }
    //Method End Program
    public static void programEnd() {
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada Fasilkom :");
        friendshipRanking();
    }
    //Method untuk mengambil/mencari Array daftarElemenFasilkom sesuai parameter
    public static ElemenFasilkom cariElemen(String nama){
        for(int i =0; i<totalElemenFasilkom;i++){
            if(daftarElemenFasilkom[i] != null){
                if(daftarElemenFasilkom[i].getNama().equals(nama)){
                    return daftarElemenFasilkom[i];
                }
            }
        }
        return null;
    } 
    //Method untuk mengambil/mencari Array daftarMataKuliah sesuai parameter
    public static MataKuliah cariMataKuliah (String nama){
        for (int i = 0; i<totalMataKuliah; i++){
            if(daftarMataKuliah[i] != null){
                if(daftarMataKuliah[i].getNama().equals(nama)){
                    return daftarMataKuliah[i];
                }
            }
        }
        return null;
    }
    //Method Getter totalElemen Fasilkom
    public static int getTotalElemen() {
        return totalElemenFasilkom;
    }
    public static void main (String[] args) {
        Scanner input = new Scanner(System.in);
        
        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                programEnd();
                break;
            }
        }
    }
}