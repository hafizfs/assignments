package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMataKuliahGUI {
    public DetailRingkasanMataKuliahGUI(JFrame frame, MataKuliah mataKuliah, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        //Panel
        JPanel panelDetailRingkasanMataKuliah = new JPanel();
        panelDetailRingkasanMataKuliah.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        
        //Label
        JLabel labelTitle = new JLabel("Detail Ringkasan Mahasiswa");
        labelTitle.setFont(SistemAkademikGUI.titelFont);
        labelTitle.setAlignmentX(Component.CENTER_ALIGNMENT); 

        JLabel labelNama = new JLabel("Nama Mata Kuliah: " + mataKuliah.getNama());
        labelNama.setFont(SistemAkademikGUI.generalFont);
        labelNama.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel labelKode = new JLabel("Kode: " + mataKuliah.getKode());
        labelKode.setFont(SistemAkademikGUI.generalFont);
        labelKode.setAlignmentX(Component.CENTER_ALIGNMENT);
    
        JLabel labelSks = new JLabel("SKS: "+ mataKuliah.getSKS());
        labelSks.setFont(SistemAkademikGUI.generalFont);
        labelSks.setAlignmentX(Component.CENTER_ALIGNMENT);
    
        JLabel labelMahasiswa = new JLabel("Jumlah Mahasiswa: "+ + mataKuliah.getJumlahMahasiswa());
        labelMahasiswa.setFont(SistemAkademikGUI.generalFont);
        labelMahasiswa.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        JLabel labelKapasitas = new JLabel("Kapasitas: " + mataKuliah.getKapasitas());
        labelKapasitas.setFont(SistemAkademikGUI.generalFont);
        labelKapasitas.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel labelDaftarMahasiswa = new JLabel("Daftar Mahasiswa:");
        labelDaftarMahasiswa.setFont(SistemAkademikGUI.generalFont);
        labelDaftarMahasiswa.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        JButton buttonSelesai = new JButton("Selesai");
        buttonSelesai.setAlignmentX(Component.CENTER_ALIGNMENT);
        buttonSelesai.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                panelDetailRingkasanMataKuliah.setVisible(false);
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        
        //Box Layout
        panelDetailRingkasanMataKuliah.setLayout(new BoxLayout(panelDetailRingkasanMataKuliah, BoxLayout.Y_AXIS));
        panelDetailRingkasanMataKuliah.add(Box.createVerticalGlue());
        panelDetailRingkasanMataKuliah.add(labelTitle);
        panelDetailRingkasanMataKuliah.add(Box.createVerticalStrut(10));
        panelDetailRingkasanMataKuliah.add(labelNama);
        panelDetailRingkasanMataKuliah.add(Box.createVerticalStrut(10));
        panelDetailRingkasanMataKuliah.add(labelKode);
        panelDetailRingkasanMataKuliah.add(Box.createVerticalStrut(10));
        panelDetailRingkasanMataKuliah.add(labelSks);
        panelDetailRingkasanMataKuliah.add(Box.createVerticalStrut(10));
        panelDetailRingkasanMataKuliah.add(labelMahasiswa);
        panelDetailRingkasanMataKuliah.add(Box.createVerticalStrut(10));
        panelDetailRingkasanMataKuliah.add(labelKapasitas);
        panelDetailRingkasanMataKuliah.add(Box.createVerticalStrut(10));
        panelDetailRingkasanMataKuliah.add(labelDaftarMahasiswa);
        panelDetailRingkasanMataKuliah.add(Box.createVerticalStrut(10));
        matkulPanel(mataKuliah, panelDetailRingkasanMataKuliah);
        panelDetailRingkasanMataKuliah.add(Box.createVerticalStrut(10));
        panelDetailRingkasanMataKuliah.add(buttonSelesai);
        panelDetailRingkasanMataKuliah.add(Box.createVerticalGlue());   

        frame.add(panelDetailRingkasanMataKuliah);
    }
    //Method handle Daftar Mahasiswa
    private void matkulPanel (MataKuliah matakuliah, JPanel panel){
        if(matakuliah.getJumlahMahasiswa()==0){
            JLabel gagal = new JLabel("Belum ada mahasiswa yang mengambil mata kuliah ini");
            gagal.setAlignmentX(Component.CENTER_ALIGNMENT);
            gagal.setFont(new Font("Verdana", Font.BOLD, 14));
            panel.add(gagal);
            panel.add(Box.createVerticalStrut(10));
        } else{
            Mahasiswa [] mahasiswa = matakuliah.getDaftarMahasiswa();
            for (int i=0 ; i< matakuliah.getJumlahMahasiswa();i++ ){
                JLabel berhasil = new JLabel((i + 1) + ". " + mahasiswa[i].getNama());
                berhasil.setAlignmentX(Component.CENTER_ALIGNMENT);
                berhasil.setFont(new Font("Verdana", Font.BOLD, 14));
                panel.add(berhasil);
                panel.add(Box.createVerticalStrut(10));
            }
        }
    }
}
