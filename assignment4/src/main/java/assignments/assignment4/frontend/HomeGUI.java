package assignments.assignment4.frontend;

import javax.swing.JFrame;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HomeGUI {
    
    public HomeGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        //Constructor
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Selamat datang di Sistem Akademik");
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.titelFont);
        
        //Panel
        JPanel panelHomeGui = new JPanel();
        panelHomeGui.setBackground(Color.decode("#ebecea"));
        panelHomeGui.setLayout(new BoxLayout(panelHomeGui, BoxLayout.Y_AXIS));
        panelHomeGui.setAlignmentX(Component.CENTER_ALIGNMENT);
        //panelHomeGui.setBackground();

        //Component & Implementasi Button

        JButton buttonTambahMhs = new JButton("Tambah Mahasiswa");
        buttonTambahMhs.setForeground(Color.decode("#eaf3d4"));
        buttonTambahMhs.setBackground(Color.decode("#9cc634"));
        buttonTambahMhs.setAlignmentX(Component.CENTER_ALIGNMENT);
        buttonTambahMhs.setFont(SistemAkademikGUI.generalFont);
        buttonTambahMhs.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent e){
                frame.remove(panelHomeGui);
                new TambahMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.setVisible(true);
            }
        });

        JButton buttonTambahMatkul = new JButton("Tambah Mata Kuliah");
        buttonTambahMatkul.setForeground(Color.decode("#eaf3d4"));
        buttonTambahMatkul.setBackground(Color.decode("#9cc634"));
        buttonTambahMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        buttonTambahMatkul.setFont(SistemAkademikGUI.generalFont);
        buttonTambahMatkul.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                frame.remove(panelHomeGui);
                new TambahMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.setVisible(true);
            }
        });
        JButton buttonTambahIRS = new JButton("Tambah IRS");
        buttonTambahIRS.setForeground(Color.decode("#eaf3d4"));
        buttonTambahIRS.setBackground(Color.decode("#9cc634"));
        buttonTambahIRS.setAlignmentX(Component.CENTER_ALIGNMENT);
        buttonTambahIRS.setFont(SistemAkademikGUI.generalFont);
        buttonTambahIRS.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                frame.remove(panelHomeGui);
                new TambahIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.setVisible(true);
            }
        });
        JButton buttonHapusIRS = new JButton("Hapus IRS");
        buttonHapusIRS.setForeground(Color.decode("#eaf3d4"));
        buttonHapusIRS.setBackground(Color.decode("#9cc634"));
        buttonHapusIRS.setAlignmentX(Component.CENTER_ALIGNMENT);
        buttonHapusIRS.setFont(SistemAkademikGUI.generalFont);
        buttonHapusIRS.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                frame.remove(panelHomeGui);
                new HapusIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.setVisible(true);
            }
        });
        JButton buttonLihatRingkasanMhs = new JButton("Lihat Ringkasan Mahasiswa");
        buttonLihatRingkasanMhs.setForeground(Color.decode("#eaf3d4"));
        buttonLihatRingkasanMhs.setBackground(Color.decode("#9cc634"));
        buttonLihatRingkasanMhs.setAlignmentX(Component.CENTER_ALIGNMENT);
        buttonLihatRingkasanMhs.setFont(SistemAkademikGUI.generalFont);
        buttonLihatRingkasanMhs.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                frame.remove(panelHomeGui);
                new RingkasanMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.setVisible(true);
            }
        });
        JButton buttonLihatRingkasanMatkul = new JButton("Lihat Ringkasan Mata Kuliah");
        buttonLihatRingkasanMatkul.setForeground(Color.decode("#eaf3d4"));
        buttonLihatRingkasanMatkul.setBackground(Color.decode("#9cc634"));
        buttonLihatRingkasanMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        buttonLihatRingkasanMatkul.setFont(SistemAkademikGUI.generalFont);
        buttonLihatRingkasanMatkul.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                frame.remove(panelHomeGui);
                new RingkasanMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.setVisible(true);
            }
        });
        
        //BoxLayout
        panelHomeGui.add(Box.createVerticalGlue());
        panelHomeGui.add(titleLabel);
        panelHomeGui.add(Box.createVerticalStrut(20));
        panelHomeGui.add(buttonTambahMhs);
        panelHomeGui.add(Box.createVerticalStrut(20));
        panelHomeGui.add(buttonTambahMatkul);
        panelHomeGui.add(Box.createVerticalStrut(20));
        panelHomeGui.add(buttonTambahIRS);
        panelHomeGui.add(Box.createVerticalStrut(20));
        panelHomeGui.add(buttonHapusIRS);
        panelHomeGui.add(Box.createVerticalStrut(20));
        panelHomeGui.add(buttonLihatRingkasanMhs);
        panelHomeGui.add(Box.createVerticalStrut(20));
        panelHomeGui.add(buttonLihatRingkasanMatkul);
        panelHomeGui.add(Box.createVerticalGlue());
        


        frame.add(panelHomeGui);


    }
}
