package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class SistemAkademik {

    
    public static void main(String[] args) { 
        new SistemAkademikGUI();
    }
}

class SistemAkademikGUI extends JFrame{
    //Datafield
    private static ArrayList<Mahasiswa> daftarMahasiswa = new ArrayList<Mahasiswa>();
    private static ArrayList<MataKuliah> daftarMataKuliah = new ArrayList<MataKuliah>();
    //Font yang akan di pakai
    public static Font generalFont = new Font("Century Gothic", Font.PLAIN , 14);
    public static Font titelFont = new Font("Century Gothic", Font.BOLD, 20);

    public SistemAkademikGUI(){

        // Constructor
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Agar program dapat di close dengan Exit
        frame.setSize(500, 500);
        frame.setTitle("Adminstrator - Sistem Akademik");
        frame.setLocationRelativeTo(null); // agar frame nya di tengah layar
        
        new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
        frame.setVisible(true);

        

    }

    //Method Getter 
    public static ArrayList<Mahasiswa> getDaftarMahasiswa() {
        return daftarMahasiswa;
    }

    public static ArrayList<MataKuliah> getDaftarMatkul() {
        return daftarMataKuliah;
    }

}
