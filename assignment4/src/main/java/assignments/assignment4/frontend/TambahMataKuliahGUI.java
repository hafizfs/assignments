package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMataKuliahGUI{

    public TambahMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        //Panel
        JPanel panelTambahMatkul = new JPanel();
        panelTambahMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        panelTambahMatkul.setBackground(Color.decode("#ebecea"));

        //Text Field
        JTextField fieldKode = new JTextField();
        fieldKode.setMaximumSize(new Dimension(200,20));
        
        JTextField fieldNamaMatkul = new JTextField();
        fieldNamaMatkul.setMaximumSize(new Dimension(200,20));
    
        JTextField fieldSks = new JTextField();
        fieldSks.setMaximumSize(new Dimension(200,20));
    
        JTextField fieldKapasitas = new JTextField();
        fieldKapasitas.setMaximumSize(new Dimension(200,20));
     
        //Label

        JLabel labelTitle = new JLabel("Tambah Mata Kuliah");
        labelTitle.setFont(SistemAkademikGUI.titelFont);
        labelTitle.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel labelKode = new JLabel("Kode Mata Kuliah: ");
        labelKode.setFont(SistemAkademikGUI.generalFont);
        labelKode.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel labelNamaMatkul = new JLabel("Nama Mata Kuliah: ");
        labelNamaMatkul.setFont(SistemAkademikGUI.generalFont);
        labelNamaMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel labelSks = new JLabel("SKS");
        labelSks.setFont(SistemAkademikGUI.generalFont);
        labelSks.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel labelKapasitas = new JLabel("Kapasitas: ");
        labelKapasitas.setFont(SistemAkademikGUI.generalFont);
        labelKapasitas.setAlignmentX(Component.CENTER_ALIGNMENT);

        //Button

        JButton buttonTambahkan = new JButton("Tambahkan");
        buttonTambahkan.setForeground(Color.decode("#eaf3d4"));
        buttonTambahkan.setBackground(Color.decode("#9cc634")); 
        buttonTambahkan.setAlignmentX(Component.CENTER_ALIGNMENT);
        buttonTambahkan.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                boolean marker = true;
                if (fieldKode.getText().equals("") || fieldNamaMatkul.getText().equals("") || fieldSks.getText().equals("") || fieldKapasitas.getText().equals("")) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                } else {
                    if(!fieldKode.getText().isEmpty()&& !fieldNamaMatkul.getText().isEmpty()&& !fieldSks.getText().isEmpty()&& !fieldKapasitas.getText().isEmpty()){
                        for(MataKuliah mataKuliah : daftarMataKuliah ){
                            //Check jika ada Matkul yang sama atau tidak
                            if(mataKuliah.getNama().equals(fieldNamaMatkul.getText())){
                                marker = false;
                                break;
                            }
                        }

                        //Check jika sudah ada Mata Kuliah atau belum
                        if (marker){
                            String kode = fieldKode.getText();
                            String nama = fieldNamaMatkul.getText();
                            int sks = Integer.parseInt(fieldSks.getText());
                            int kapasitas = Integer.parseInt(fieldKapasitas.getText());

                            MataKuliah mataKuliah = new MataKuliah(kode, nama, sks, kapasitas);
                            daftarMataKuliah.add(mataKuliah);

                            JOptionPane.showMessageDialog(frame, "Mata kuliah " + nama + " berhasil ditambahkan");
                            //Reset
                            fieldKode.setText("");
                            fieldNamaMatkul.setText("");
                            fieldSks.setText("");
                            fieldKapasitas.setText("");
                        } else{
                            JOptionPane.showMessageDialog(frame, "Mata Kuliah " + fieldNamaMatkul.getText() + " sudah pernah ditambahkan sebelumnya"); 
                            //Reset
                            fieldKode.setText("");
                            fieldNamaMatkul.setText("");
                            fieldSks.setText("");
                            fieldKapasitas.setText("");
                        }
                    }
                }
            }
        });
        JButton buttonKembali = new JButton("Kembali");
        buttonKembali.setForeground(Color.decode("#eaf3d4"));
        buttonKembali.setBackground(Color.decode("#1FBED6"));
        buttonKembali.setAlignmentX(Component.CENTER_ALIGNMENT);
        buttonKembali.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                panelTambahMatkul.setVisible(false);
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });

        //Box Layout
        panelTambahMatkul.setLayout(new BoxLayout(panelTambahMatkul, BoxLayout.Y_AXIS));
        panelTambahMatkul.add(Box.createVerticalGlue());
        panelTambahMatkul.add(labelTitle);
        panelTambahMatkul.add(Box.createVerticalStrut(10));
        panelTambahMatkul.add(labelKode);
        panelTambahMatkul.add(Box.createVerticalStrut(10));
        panelTambahMatkul.add(fieldKode);
        panelTambahMatkul.add(Box.createVerticalStrut(10));
        panelTambahMatkul.add(labelNamaMatkul);
        panelTambahMatkul.add(Box.createVerticalStrut(10));
        panelTambahMatkul.add(fieldNamaMatkul);
        panelTambahMatkul.add(Box.createVerticalStrut(10));
        panelTambahMatkul.add(labelSks);
        panelTambahMatkul.add(Box.createVerticalStrut(10));
        panelTambahMatkul.add(fieldSks);
        panelTambahMatkul.add(Box.createVerticalStrut(10));
        panelTambahMatkul.add(labelKapasitas);
        panelTambahMatkul.add(Box.createVerticalStrut(10));
        panelTambahMatkul.add(fieldKapasitas);
        panelTambahMatkul.add(Box.createVerticalStrut(10));
        panelTambahMatkul.add(buttonTambahkan);
        panelTambahMatkul.add(Box.createVerticalStrut(10));
        panelTambahMatkul.add(buttonKembali);
        panelTambahMatkul.add(Box.createVerticalGlue());

        frame.add(panelTambahMatkul);
    }
    
}
