package assignments.assignment4.frontend;

import java.awt.*;

import javax.naming.NameNotFoundException;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JOptionPane;
import javax.swing.border.EmptyBorder;

import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMahasiswaGUI{

    public TambahMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        
        //Panel
        JPanel panelTambahMahasiswa = new JPanel();
        panelTambahMahasiswa.setAlignmentX(Component.CENTER_ALIGNMENT);
        panelTambahMahasiswa.setBackground(Color.decode("#ebecea"));
        //TextField
        JTextField fieldNama = new JTextField();
        fieldNama.setMaximumSize(new Dimension(200,20));
        
        JTextField fieldNpm = new JTextField(17);
        fieldNpm.setMaximumSize(new Dimension(200,20));
        

        //Label
        JLabel labelNama = new JLabel("Nama: ");
        labelNama.setFont(SistemAkademikGUI.generalFont);
        labelNama.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        JLabel labelNpm = new JLabel("NPM: ");
        labelNpm.setFont(SistemAkademikGUI.generalFont);
        labelNpm.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        JLabel labelTitle = new JLabel("Tambah Mahasiswa");
        labelTitle.setFont(SistemAkademikGUI.titelFont);
        labelTitle.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelTitle.setHorizontalAlignment(JLabel.CENTER);

        //Button
        JButton buttonTambahkan = new JButton("Tambahkan");
        buttonTambahkan.setAlignmentX(Component.CENTER_ALIGNMENT);
        buttonTambahkan.setForeground(Color.decode("#eaf3d4"));
        buttonTambahkan.setBackground(Color.decode("#9cc634")); 
        //buttonTambahkan.setBackground(Color.decode("#9cc634"));
        buttonTambahkan.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                boolean marker = true;
                if (fieldNama.getText().equals("") || fieldNpm.getText().equals("")){
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                } else{
                    //Check jika npm sudah sama atau belum
                    if(!fieldNama.getText().isEmpty()&& !fieldNpm.getText().isEmpty()){
                        for(Mahasiswa mahasiswa : daftarMahasiswa){
                            if(mahasiswa.getNpm() == Long.parseLong(fieldNpm.getText())){
                                marker = false;
                                break;
                            }
                        }
                        //Check jika NPM sudah ada atau belum
                        if (marker){
                            Mahasiswa mahasiswa = new Mahasiswa(fieldNama.getText(), Long.parseLong(fieldNpm.getText()));
                            daftarMahasiswa.add(mahasiswa);
                            JOptionPane.showMessageDialog(frame, "Mahasiswa " + Long.parseLong(fieldNpm.getText()) + "-" + fieldNama.getText() + " berhasil ditambahkan");
                            fieldNama.setText("");
                            fieldNpm.setText("");
                        } else{
                            JOptionPane.showMessageDialog(frame, "NPM " + fieldNpm.getText()  + " sudah pernah ditambahkan sebelumnya");
                            fieldNama.setText("");
                            fieldNpm.setText("");
                        }
                    }
                }

            }
        });
        //Handle Button Kembali
        JButton buttonKembali = new JButton("Kembali");
        buttonKembali.setForeground(Color.decode("#eaf3d4"));
        buttonKembali.setBackground(Color.decode("#1FBED6"));
        buttonKembali.setAlignmentX(Component.CENTER_ALIGNMENT);
        buttonKembali.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                panelTambahMahasiswa.setVisible(false);
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });

        
        //Box layout
        panelTambahMahasiswa.setLayout(new BoxLayout(panelTambahMahasiswa, BoxLayout.Y_AXIS));
        panelTambahMahasiswa.add(Box.createVerticalGlue());
        panelTambahMahasiswa.add(labelTitle);
        panelTambahMahasiswa.add(Box.createVerticalStrut(10));
        panelTambahMahasiswa.add(labelNama);
        panelTambahMahasiswa.add(Box.createVerticalStrut(10));
        panelTambahMahasiswa.add(fieldNama);
        panelTambahMahasiswa.add(Box.createVerticalStrut(10));
        panelTambahMahasiswa.add(labelNpm);
        panelTambahMahasiswa.add(Box.createVerticalStrut(10));
        panelTambahMahasiswa.add(fieldNpm);
        panelTambahMahasiswa.add(Box.createVerticalStrut(10));
        panelTambahMahasiswa.add(buttonTambahkan);
        panelTambahMahasiswa.add(Box.createVerticalStrut(10));
        panelTambahMahasiswa.add(buttonKembali);
        panelTambahMahasiswa.add(Box.createVerticalGlue());

        frame.add(panelTambahMahasiswa);
    }
    
}
