package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMataKuliahGUI {

    public RingkasanMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        
        //Panel
        JPanel panelRingkasanMataKuliah = new JPanel();
        panelRingkasanMataKuliah.setAlignmentX(Component.CENTER_ALIGNMENT);
        panelRingkasanMataKuliah.setBackground(Color.decode("#ebecea"));

        //Combobox
        JComboBox boxMatkul = new JComboBox(getSortingMatkulArray());
        boxMatkul.setMaximumSize(new Dimension (200,20));

        //Label
        JLabel labelTitle = new JLabel("Ringkasan Mata Kuliah");
        labelTitle.setFont(SistemAkademikGUI.titelFont);
        labelTitle.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        JLabel labelPilihMatkul = new JLabel("Pilih Mata Kuliah");
        labelPilihMatkul.setFont(SistemAkademikGUI.generalFont);
        labelPilihMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        JButton buttonLihat = new JButton("Lihat");
        buttonLihat.setForeground(Color.decode("#eaf3d4"));
        buttonLihat.setBackground(Color.decode("#9cc634")); 
        buttonLihat.setAlignmentX(Component.CENTER_ALIGNMENT);
        buttonLihat.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                String stringMatkul = (String) boxMatkul.getSelectedItem();
                if (stringMatkul != null){
                    MataKuliah matakuliah = getMataKuliah(stringMatkul);
                    new DetailRingkasanMataKuliahGUI(frame, matakuliah, daftarMahasiswa, daftarMataKuliah);
                    frame.setVisible(true);
                    frame.remove(panelRingkasanMataKuliah);
                } else {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                }
            }
        });
        
        
    
        JButton buttonKembali = new JButton("Kembali");
        buttonKembali.setForeground(Color.decode("#eaf3d4"));
        buttonKembali.setBackground(Color.decode("#1FBED6"));
        buttonKembali.setAlignmentX(Component.CENTER_ALIGNMENT);
        buttonKembali.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                panelRingkasanMataKuliah.setVisible(false);
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        //Box Layout
        panelRingkasanMataKuliah.setLayout(new BoxLayout(panelRingkasanMataKuliah, BoxLayout.Y_AXIS));
        panelRingkasanMataKuliah.add(Box.createVerticalGlue());
        panelRingkasanMataKuliah.add(labelTitle);
        panelRingkasanMataKuliah.add(Box.createVerticalStrut(10));
        panelRingkasanMataKuliah.add(labelPilihMatkul);
        panelRingkasanMataKuliah.add(Box.createVerticalStrut(10));
        panelRingkasanMataKuliah.add(boxMatkul);
        panelRingkasanMataKuliah.add(Box.createVerticalStrut(10));
        panelRingkasanMataKuliah.add(buttonLihat);
        panelRingkasanMataKuliah.add(Box.createVerticalStrut(10));
        panelRingkasanMataKuliah.add(buttonKembali);
        panelRingkasanMataKuliah.add(Box.createVerticalGlue());

        frame.add(panelRingkasanMataKuliah); 
    }

   //Method Sorting Matkul Array
    private String[] getSortingMatkulArray(){
        ArrayList<MataKuliah> daftarMatkul = SistemAkademikGUI.getDaftarMatkul(); //Array List Matkul
        String [] arrayMatkul = new String[daftarMatkul.size()]; //Matkul Array
        //for loop Sorting Array Matkul sesuai dengan alphabet
        for (int i = 0; i < daftarMatkul.size(); i++) {
            for (int j = i + 1; j < daftarMatkul.size(); j++) {
                MataKuliah temp;
                if (daftarMatkul.get(i).getNama().compareTo(daftarMatkul.get(j).getNama()) > 0) {
                    temp = daftarMatkul.get(i);
                    daftarMatkul.set(i, daftarMatkul.get(j));
                    daftarMatkul.set(j, temp);
                }
            }
        }
        //For loop untuk nge print Array
        for (int i = 0; i < arrayMatkul.length; i++) {
            arrayMatkul[i] = daftarMatkul.get(i).getNama();
        }
        return arrayMatkul;
    }
   //Method getter Mata Kuliah
    private MataKuliah getMataKuliah(String nama) {

        for (MataKuliah mataKuliah : SistemAkademikGUI.getDaftarMatkul()) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }
}
