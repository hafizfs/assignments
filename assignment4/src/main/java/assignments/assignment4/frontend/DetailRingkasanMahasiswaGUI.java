package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMahasiswaGUI {
    public DetailRingkasanMahasiswaGUI(JFrame frame, Mahasiswa mahasiswa, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        //Panel
        JPanel panelDetailRingkasanMahasiswa = new JPanel();
        panelDetailRingkasanMahasiswa.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        //Label
        JLabel labelTitle = new JLabel("Detail Ringkasan Mahasiswa");
        labelTitle.setFont(SistemAkademikGUI.titelFont);
        labelTitle.setAlignmentX(Component.CENTER_ALIGNMENT); 

        JLabel labelNama = new JLabel("Nama: " + mahasiswa.getNama());
        labelNama.setFont(SistemAkademikGUI.generalFont);
        labelNama.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel labelNpm = new JLabel("NPM: " + mahasiswa.getNpm());
        labelNpm.setFont(SistemAkademikGUI.generalFont);
        labelNpm.setAlignmentX(Component.CENTER_ALIGNMENT);
    
        JLabel labelJurusan = new JLabel("Jurusan: "+ mahasiswa.getJurusan());
        labelJurusan.setFont(SistemAkademikGUI.generalFont);
        labelJurusan.setAlignmentX(Component.CENTER_ALIGNMENT);
    
        JLabel labelMatkul = new JLabel("Daftar Mata Kuliah:");
        labelMatkul.setFont(SistemAkademikGUI.generalFont);
        labelMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        JLabel labelSks = new JLabel("Total SKS: " + mahasiswa.getTotalSKS());
        labelSks.setFont(SistemAkademikGUI.generalFont);
        labelSks.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel labelHasil = new JLabel("Hasil Pengecekan IRS:");
        labelHasil.setFont(SistemAkademikGUI.generalFont);
        labelHasil.setAlignmentX(Component.CENTER_ALIGNMENT);

        JButton buttonSelesai = new JButton("Selesai");
        buttonSelesai.setAlignmentX(Component.CENTER_ALIGNMENT);
        buttonSelesai.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                panelDetailRingkasanMahasiswa.setVisible(false);
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        }); 
        panelDetailRingkasanMahasiswa.setLayout(new BoxLayout(panelDetailRingkasanMahasiswa, BoxLayout.Y_AXIS));
        panelDetailRingkasanMahasiswa.add(Box.createVerticalGlue());
        panelDetailRingkasanMahasiswa.add(labelTitle);
        panelDetailRingkasanMahasiswa.add(Box.createVerticalStrut(10));
        panelDetailRingkasanMahasiswa.add(labelNama);
        panelDetailRingkasanMahasiswa.add(Box.createVerticalStrut(10));
        panelDetailRingkasanMahasiswa.add(labelNpm);
        panelDetailRingkasanMahasiswa.add(Box.createVerticalStrut(10));
        panelDetailRingkasanMahasiswa.add(labelJurusan);
        panelDetailRingkasanMahasiswa.add(Box.createVerticalStrut(10));
        panelDetailRingkasanMahasiswa.add(labelMatkul);
        panelDetailRingkasanMahasiswa.add(Box.createVerticalStrut(10));
        matkulPanel(mahasiswa, panelDetailRingkasanMahasiswa);
        panelDetailRingkasanMahasiswa.add(Box.createVerticalStrut(10));
        panelDetailRingkasanMahasiswa.add(labelSks);
        panelDetailRingkasanMahasiswa.add(Box.createVerticalStrut(10));
        panelDetailRingkasanMahasiswa.add(labelHasil);
        panelDetailRingkasanMahasiswa.add(Box.createVerticalStrut(10));
        problemPanel(mahasiswa, panelDetailRingkasanMahasiswa);
        panelDetailRingkasanMahasiswa.add(Box.createVerticalStrut(10));
        panelDetailRingkasanMahasiswa.add(buttonSelesai);
        panelDetailRingkasanMahasiswa.add(Box.createVerticalGlue());   

        frame.add(panelDetailRingkasanMahasiswa);
    
    }
    //Method handle Daftar Mata Kuliah
    private void matkulPanel (Mahasiswa mahasiswa, JPanel panel){
        if(mahasiswa.getBanyakMatkul()==0){
            JLabel gagal = new JLabel("Belum ada mata kuliah yang diambil");
            gagal.setAlignmentX(Component.CENTER_ALIGNMENT);
            gagal.setFont(new Font("Verdana", Font.BOLD, 14));
            panel.add(gagal);
        } else{
            MataKuliah [] matakuliah = mahasiswa.getMataKuliah();
            for (int i=0 ; i< mahasiswa.getBanyakMatkul();i++ ){
                JLabel berhasil = new JLabel((i + 1) + ". " + matakuliah[i].getNama());
                berhasil.setAlignmentX(Component.CENTER_ALIGNMENT);
                berhasil.setFont(new Font("Verdana", Font.BOLD, 14));
                panel.add(berhasil);
            }
        }
    }
    //Method untuk handle check IRS
    private void problemPanel (Mahasiswa mahasiswa, JPanel panel){
        mahasiswa.cekIRS();
        if(mahasiswa.getBanyakMatkul()==0){
            JLabel gagal = new JLabel("IRS tidak bermasalah");
            gagal.setAlignmentX(Component.CENTER_ALIGNMENT);
            gagal.setFont(new Font("Verdana", Font.BOLD, 14));
            panel.add(gagal);
            panel.add(Box.createVerticalStrut(10));
        } else {
            String[] problems = mahasiswa.getMasalahIRS();
            for (int i =0; i < mahasiswa.getBanyakMasalahIRS(); i++){
                JLabel berhasil = new JLabel((i + 1) + ". " + problems[i]);
                berhasil.setAlignmentX(Component.CENTER_ALIGNMENT);
                berhasil.setFont(new Font("Verdana", Font.BOLD, 14));
                panel.add(berhasil);
                panel.add(Box.createVerticalStrut(10));
            }


        }
}
}
