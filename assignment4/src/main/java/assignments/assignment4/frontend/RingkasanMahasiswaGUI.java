package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMahasiswaGUI {

    public RingkasanMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        //Panel
        JPanel panelRingkasanMahasiswa = new JPanel();
        panelRingkasanMahasiswa.setAlignmentX(Component.CENTER_ALIGNMENT);
        panelRingkasanMahasiswa.setBackground(Color.decode("#ebecea"));

        //Combobox 
        JComboBox boxNpm = new JComboBox(getSortingNPMArray());
        boxNpm.setMaximumSize(new Dimension (200,20));

        //Label
        JLabel labelTitle = new JLabel("Ringkasan Mahasiswa");
        labelTitle.setFont(SistemAkademikGUI.titelFont);
        labelTitle.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        JLabel labelPilihNpm = new JLabel("Pilih NPM");
        labelPilihNpm.setFont(SistemAkademikGUI.generalFont);
        labelPilihNpm.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        JButton buttonLihat = new JButton("Lihat");
        buttonLihat.setForeground(Color.decode("#eaf3d4"));
        buttonLihat.setBackground(Color.decode("#9cc634")); 
        buttonLihat.setAlignmentX(Component.CENTER_ALIGNMENT);
        buttonLihat.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                String stringNpm = (String) boxNpm.getSelectedItem();
                if (stringNpm == null){
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                } else {
                    long npm = Long.parseLong(stringNpm);
                    Mahasiswa mahasiswa = getMahasiswa(npm);
                    new DetailRingkasanMahasiswaGUI(frame, mahasiswa, daftarMahasiswa, daftarMataKuliah);
                    frame.setVisible(true);
                    panelRingkasanMahasiswa.setVisible(false);
                }
            }
        });
        
        
        JButton buttonKembali = new JButton("Kembali");
        buttonKembali.setForeground(Color.decode("#eaf3d4"));
        buttonKembali.setBackground(Color.decode("#1FBED6"));
        buttonKembali.setAlignmentX(Component.CENTER_ALIGNMENT);
        buttonKembali.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                panelRingkasanMahasiswa.setVisible(false);
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        
        
        //Box Layout
        panelRingkasanMahasiswa.setLayout(new BoxLayout(panelRingkasanMahasiswa, BoxLayout.Y_AXIS));
        panelRingkasanMahasiswa.add(Box.createVerticalGlue());
        panelRingkasanMahasiswa.add(labelTitle);
        panelRingkasanMahasiswa.add(Box.createVerticalStrut(10));
        panelRingkasanMahasiswa.add(labelPilihNpm);
        panelRingkasanMahasiswa.add(Box.createVerticalStrut(10));
        panelRingkasanMahasiswa.add(boxNpm);
        panelRingkasanMahasiswa.add(Box.createVerticalStrut(10));
        panelRingkasanMahasiswa.add(buttonLihat);
        panelRingkasanMahasiswa.add(Box.createVerticalStrut(10));
        panelRingkasanMahasiswa.add(buttonKembali);
        panelRingkasanMahasiswa.add(Box.createVerticalGlue());

        frame.add(panelRingkasanMahasiswa);
    }
    //Method Sorting NPM Array
    private String[] getSortingNPMArray() {
        ArrayList<Mahasiswa> daftarMahasiswa = SistemAkademikGUI.getDaftarMahasiswa();
        String[] strNpm = new String[daftarMahasiswa.size()];
        //for loop Sorting Array dari kecil ke gede
        for (int i = 0; i < daftarMahasiswa.size(); i++) {
            for (int j = i + 1; j < daftarMahasiswa.size(); j++) {
                Mahasiswa temp;
                if (daftarMahasiswa.get(i).getNpm() > daftarMahasiswa.get(j).getNpm()) {
                    temp = daftarMahasiswa.get(i);
                    daftarMahasiswa.set(i, daftarMahasiswa.get(j));
                    daftarMahasiswa.set(j, temp);
                }
            }
        }
        //looping untuk mereturn hasil String NPM ke Combobox
        for (int i = 0; i < strNpm.length; i++) {
            strNpm[i] = String.valueOf(daftarMahasiswa.get(i).getNpm());
        }
        
        return strNpm;
    }


    //Method Getter Mahasiswa
    private Mahasiswa getMahasiswa(long npm) {
        for (Mahasiswa mahasiswa : SistemAkademikGUI.getDaftarMahasiswa()) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }
}
