package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HapusIRSGUI {

    public HapusIRSGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        //Panel
        JPanel panelHapusIRS = new JPanel();
        panelHapusIRS.setAlignmentX(Component.CENTER_ALIGNMENT);
        panelHapusIRS.setBackground(Color.decode("#ebecea"));
        
        //Combobox
        JComboBox boxNpm = new JComboBox<String>();
        boxNpm.setMaximumSize(new Dimension (200,20));
        for (int i = 0; i<daftarMahasiswa.size();i++){
            long temp = daftarMahasiswa.get(i).getNpm();
            boxNpm.addItem(Long.toString(temp));
        }
        //Combobox
        JComboBox boxMatkul = new JComboBox<String>();
        boxMatkul.setMaximumSize(new Dimension (200,20));
        for (int i = 0; i<daftarMataKuliah.size();i++){
            String temp = daftarMataKuliah.get(i).getNama();
            boxMatkul.addItem(temp.toString());
        }
        
        //Label
        JLabel labelTitle = new JLabel("Tambah IRS");
        labelTitle.setFont(SistemAkademikGUI.titelFont);
        labelTitle.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel labelPilihNpm = new JLabel("Pilih NPM");
        labelPilihNpm.setFont(SistemAkademikGUI.generalFont);
        labelPilihNpm.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel labelPilihMatkul = new JLabel("Pilih Nama Matkul");
        labelPilihMatkul.setFont(SistemAkademikGUI.generalFont);
        labelPilihMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);

        JButton buttonTambahkan = new JButton("Hapus");
        buttonTambahkan.setForeground(Color.decode("#eaf3d4"));
        buttonTambahkan.setBackground(Color.decode("#9cc634")); 
        buttonTambahkan.setAlignmentX(Component.CENTER_ALIGNMENT);
        buttonTambahkan.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                String stringNpm = (String) boxNpm.getSelectedItem();
                String stringMatkul = (String) boxMatkul.getSelectedItem();
                if (stringNpm == null ||stringMatkul == null){
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                } else{
                    long npm = Long.parseLong(stringNpm);
                    Mahasiswa mahasiswa = getMahasiswa(npm);
                    MataKuliah mataKuliah = getMataKuliah(stringMatkul);
                    JOptionPane.showMessageDialog(frame, mahasiswa.dropMatkul(mataKuliah));

                }
            }
        });
        
        JButton buttonKembali = new JButton("Kembali");
        buttonKembali.setForeground(Color.decode("#eaf3d4"));
        buttonKembali.setBackground(Color.decode("#1FBED6"));
        buttonKembali.setAlignmentX(Component.CENTER_ALIGNMENT);
        buttonKembali.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                panelHapusIRS.setVisible(false);
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        //Box Layout
        panelHapusIRS.setLayout(new BoxLayout(panelHapusIRS, BoxLayout.Y_AXIS));
        panelHapusIRS.add(Box.createVerticalGlue());
        panelHapusIRS.add(labelTitle);
        panelHapusIRS.add(Box.createVerticalStrut(10));
        panelHapusIRS.add(labelPilihNpm);
        panelHapusIRS.add(Box.createVerticalStrut(10));
        panelHapusIRS.add(boxNpm);
        panelHapusIRS.add(Box.createVerticalStrut(10));
        panelHapusIRS.add(labelPilihMatkul);
        panelHapusIRS.add(Box.createVerticalStrut(10));
        panelHapusIRS.add(boxMatkul);
        panelHapusIRS.add(Box.createVerticalStrut(10));
        panelHapusIRS.add(buttonTambahkan);
        panelHapusIRS.add(Box.createVerticalStrut(10));
        panelHapusIRS.add(buttonKembali);
        panelHapusIRS.add(Box.createVerticalGlue());

        frame.add(panelHapusIRS);
    

    }   
    //Method Getter
    private MataKuliah getMataKuliah(String nama) {

        for (MataKuliah mataKuliah : SistemAkademikGUI.getDaftarMatkul()) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }

    private Mahasiswa getMahasiswa(long npm) {

        for (Mahasiswa mahasiswa : SistemAkademikGUI.getDaftarMahasiswa()) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }
}
