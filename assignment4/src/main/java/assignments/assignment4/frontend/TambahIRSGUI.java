package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahIRSGUI {

    public TambahIRSGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        //Panel
        JPanel panelTambahIRS = new JPanel();
        panelTambahIRS.setAlignmentX(Component.CENTER_ALIGNMENT);
        panelTambahIRS.setBackground(Color.decode("#ebecea"));

        //Combobox
        JComboBox boxNpm = new JComboBox<String>();
        boxNpm.setMaximumSize(new Dimension (200,20));

        for (int i = 0; i<daftarMahasiswa.size();i++){
            long temp = daftarMahasiswa.get(i).getNpm();
            boxNpm.addItem(Long.toString(temp));
        }

        JComboBox boxMatkul = new JComboBox<String>();
        boxMatkul.setMaximumSize(new Dimension (200,20));
        for (int i = 0; i<daftarMataKuliah.size();i++){
            String temp = daftarMataKuliah.get(i).getNama();
            boxMatkul.addItem(temp.toString());
        }

        //Label
        JLabel labelTitle = new JLabel("Tambah IRS");
        labelTitle.setFont(SistemAkademikGUI.titelFont);
        labelTitle.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel labelPilihNpm = new JLabel("Pilih NPM");
        labelPilihNpm.setFont(SistemAkademikGUI.generalFont);
        labelPilihNpm.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel labelPilihMatkul = new JLabel("Pilih Nama Matkul");
        labelPilihMatkul.setFont(SistemAkademikGUI.generalFont);
        labelPilihMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);

        JButton buttonTambahkan = new JButton("Tambahkan");
        buttonTambahkan.setForeground(Color.decode("#eaf3d4"));
        buttonTambahkan.setBackground(Color.decode("#9cc634")); 
        buttonTambahkan.setAlignmentX(Component.CENTER_ALIGNMENT);
        buttonTambahkan.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                String stringNpm = (String) boxNpm.getSelectedItem();
                String stringMatkul = (String) boxMatkul.getSelectedItem();
                if (stringNpm == null ||stringMatkul == null){
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                } else{
                    long npm = Long.parseLong(stringNpm);
                    Mahasiswa mahasiswa = getMahasiswa(npm);
                    MataKuliah mataKuliah = getMataKuliah(stringMatkul);
                    JOptionPane.showMessageDialog(frame, mahasiswa.addMatkul(mataKuliah));

                }
            }
        });
        JButton buttonKembali = new JButton("Kembali");
        buttonKembali.setForeground(Color.decode("#eaf3d4"));
        buttonKembali.setBackground(Color.decode("#1FBED6"));
        buttonKembali.setAlignmentX(Component.CENTER_ALIGNMENT);
        buttonKembali.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                panelTambahIRS.setVisible(false);
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });

        //Box Layout
        panelTambahIRS.setLayout(new BoxLayout(panelTambahIRS, BoxLayout.Y_AXIS));
        panelTambahIRS.add(Box.createVerticalGlue());
        panelTambahIRS.add(labelTitle);
        panelTambahIRS.add(Box.createVerticalStrut(10));
        panelTambahIRS.add(labelPilihNpm);
        panelTambahIRS.add(Box.createVerticalStrut(10));
        panelTambahIRS.add(boxNpm);
        panelTambahIRS.add(Box.createVerticalStrut(10));
        panelTambahIRS.add(labelPilihMatkul);
        panelTambahIRS.add(Box.createVerticalStrut(10));
        panelTambahIRS.add(boxMatkul);
        panelTambahIRS.add(Box.createVerticalStrut(10));
        panelTambahIRS.add(buttonTambahkan);
        panelTambahIRS.add(Box.createVerticalStrut(10));
        panelTambahIRS.add(buttonKembali);
        panelTambahIRS.add(Box.createVerticalGlue());

        frame.add(panelTambahIRS);
    
    }

    
    private MataKuliah getMataKuliah(String nama) {

        for (MataKuliah mataKuliah : SistemAkademikGUI.getDaftarMatkul()) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }

    private Mahasiswa getMahasiswa(long npm) {

        for (Mahasiswa mahasiswa : SistemAkademikGUI.getDaftarMahasiswa()) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }
}
